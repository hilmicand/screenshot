﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScreenShot.Properties;

namespace ScreenShot
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            this.TopMost = true;
            
            pictureBox1.Size = this.Size;
            
            if (Settings.Default.kayit == true)
            {
                pictureBox1.Load(Settings.Default.file_name);
            }
            else 
            {
                pictureBox1.Load("dynamic.hlm");
            }



            if (Settings.Default.En == true)
            {
                button1.Text = English.Class1.don;
            }
            else 
            {
                button1.Text = Turkce.Class1.don;
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        
        
    }
}
