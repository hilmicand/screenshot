﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScreenShot.Properties;

namespace ScreenShot
{
    public partial class Ayarlar : Form
    {
        public Ayarlar()
        {
            InitializeComponent();
        }

        private string a;
        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Tr  = radioButton1.Checked;
            Properties.Settings.Default.En = radioButton2.Checked;
            Properties.Settings.Default.splash = checkBox1.Checked;
            Properties.Settings.Default.Save();

            Restart r = new Restart();
            r.Show();
        }

        private void Ayarlar_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = Properties.Settings.Default.Tr;
            radioButton2.Checked = Properties.Settings.Default.En;
            checkBox1.Checked = Properties.Settings.Default.splash;

            if (Settings.Default.En == false)
            {
                groupBox1.Text = Turkce.Class1.dil_ayar;
                groupBox2.Text = Turkce.Class1.diger;
                label1.Text = Turkce.Class1.ana_dil;
                checkBox1.Text = Turkce.Class1.splash_kullan;
                button1.Text = Turkce.Class1.kaydet;
                button2.Text = Turkce.Class1.iptal;
            }
            else 
            {
                groupBox1.Text = English.Class1.dil_ayar;
                groupBox2.Text = English.Class1.diger;
                label1.Text = English.Class1.ana_dil;
                checkBox1.Text = English.Class1.splash_kullan;
                button1.Text = English.Class1.kaydet;
                button2.Text = English.Class1.iptal;

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
