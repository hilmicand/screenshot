﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScreenShot
{
    public partial class Restart : Form
    {
        public Restart()
        {
            InitializeComponent();
        }

        private void Restart_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1500;
            timer1.Start();
            label1.Text = "Ayarlar Uygulanıyor... / Settings are applying...";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "Yeniden başlatma için hazırlanıyor... / Preparing for restart...";
            timer1.Stop();
            timer2.Interval = 800;
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value += 50;
            }
            if(progressBar1.Value == 100) 
            {
                timer2.Stop();
                Application.Restart();
            }
        }
    }
}
