﻿
// Program Name : ScreenShot!
// Author : HilmiCanDelibas
// Creating Date : 24.09.2014
// Updating Date : 11.05.2015
// Version(at the moment) : 3.5.5.0



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using ScreenShot.Properties;

namespace ScreenShot_
{
    public partial class Form1 : Form
    {

        private string baslik = "ScreenShot!";
        private string uyri_msj = "Bu özellik geliştirme aşamasında olduğundan şimdilik devre dışı!";
        private string uyari = "Uyarı";
        private string msj = "Yakaladığınız ekran görüntüsü varsa kaydedilmeyecek\nDevam etmek istiyor musunuz?";
        private string tool = "Ekran görüntüsü almak için bu ikona tıklayın!";
        private string save_image = "Görüntüyü Kaydet";


        private void DilTurkce() 
        {
            button1.Text = Turkce.Class1.yakala;
            button3.Text = Turkce.Class1.kaydet;
            button2.Text = Turkce.Class1.edit;
            menüToolStripMenuItem.Text = Turkce.Class1.menu;
            yeniÇekimToolStripMenuItem.Text = Turkce.Class1.yakala;
            resmiKaydetToolStripMenuItem.Text = Turkce.Class1.kaydet;
            çıkışToolStripMenuItem.Text = Turkce.Class1.cik;
            seçeneklerToolStripMenuItem.Text = Turkce.Class1.secenek;
            hakkındaToolStripMenuItem.Text = Turkce.Class1.about;
            yardımToolStripMenuItem.Text = Turkce.Class1.yrdm;
            label1.Text = Turkce.Class1.uyari;
            baslik = Turkce.Class1.baslik;
            msj = Turkce.Class1.msj_text;
            uyari = Turkce.Class1.msj_baslik;
            uyri_msj = Turkce.Class1.msj;
            tool = Turkce.Class1.tooltip;
            save_image = Turkce.Class1.kydt;
            resmiGörüntüleToolStripMenuItem.Text = Turkce.Class1.rturn;
            screenShotHakkındaToolStripMenuItem.Text = Turkce.Class1.about_;
            çıkToolStripMenuItem.Text = Turkce.Class1.cik;
            button4.Text = Turkce.Class1.fscr;
            ayarlarToolStripMenuItem.Text = Turkce.Class1.ayar;
        }

        private void Eng()
        {
            button1.Text = English.Class1.yakala;
            button3.Text = English.Class1.kaydet;
            button2.Text = English.Class1.edit;
            menüToolStripMenuItem.Text = English.Class1.menu;
            yeniÇekimToolStripMenuItem.Text = English.Class1.yakala;
            resmiKaydetToolStripMenuItem.Text = English.Class1.kaydet;
            çıkışToolStripMenuItem.Text = English.Class1.cik;
            seçeneklerToolStripMenuItem.Text = English.Class1.secenek;
            hakkındaToolStripMenuItem.Text = English.Class1.about;
            yardımToolStripMenuItem.Text = English.Class1.yrdm;
            label1.Text = English.Class1.uyari;
            baslik = English.Class1.baslik;
            msj = English.Class1.msj_text;
            uyari = English.Class1.msj_baslik;
            uyri_msj = English.Class1.msj;
            tool = English.Class1.tooltip;
            save_image = English.Class1.kydt;
            resmiGörüntüleToolStripMenuItem.Text = English.Class1.rturn;
            screenShotHakkındaToolStripMenuItem.Text = English.Class1.about_;
            çıkToolStripMenuItem.Text = English.Class1.cik;
            button4.Text = English.Class1.fscr;
            ayarlarToolStripMenuItem.Text = English.Class1.ayar;
        }

        public Form1()
        {
            InitializeComponent();
        }

       

        public static Bitmap Görüntü()
        {
            Bitmap Görüntü = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics GFX = Graphics.FromImage(Görüntü);
            GFX.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size);
            return Görüntü;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            notifyIcon1.ShowBalloonTip(20, baslik, tool, ToolTipIcon.Info);
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            pictureBox1.Image = Görüntü();
            this.Show();
            button4.Enabled = true;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            label1.SendToBack();
            button2.Enabled = true;
            button3.Enabled = true;
            resmiKaydetToolStripMenuItem.Enabled = true;
            pictureBox1.Image.Save("dynamic.hlm");
        }


        private void Kaydet() 
        {

            SaveFileDialog save = new SaveFileDialog();
            save.Title = save_image;
            save.Filter = "(*.jpg)|*.jpg|(*.png)|*.png|(*.bmp)|*.bmp|Tüm dosyalar(*.*)|*.*";
            saveFileDialog1.InitialDirectory = "C:\\";
            ImageFormat dosya_formatı = ImageFormat.Bmp;
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ad = System.IO.Path.GetExtension(save.FileName);
                switch (ad)
                {
                    case ".jpg":
                        dosya_formatı = ImageFormat.Bmp;
                        break;

                    case ".png":
                        dosya_formatı = ImageFormat.Png;
                        break;

                    case ".bmp":
                        dosya_formatı = ImageFormat.Bmp;
                        break;
                }
                pictureBox1.Image.Save(save.FileName, dosya_formatı);

                Settings.Default.kayit = true;
                Settings.Default.file_name = save.FileName;
                Settings.Default.Save();
            }

            this.Text += " - " + save.FileName;

            
        }


        
        private void resmiKaydetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Kaydet();
        }

        private void resmiGörüntüleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void yeniÇekimToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            notifyIcon1.ShowBalloonTip(20, baslik, tool, ToolTipIcon.Info);
            button4.Enabled = true;
        }

        private void hakkındaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm();
        }

        private void çıkışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(uyri_msj, uyari, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void button3_Click(object sender, EventArgs e)
        {
            Kaydet();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (ScreenShot.Properties.Settings.Default.Tr == true)
            {
                DilTurkce();
            }
            else
            {
                Eng();
            }

            resmiKaydetToolStripMenuItem.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;

            ScreenShot.Properties.Settings.Default.kayit = false;
        }

        private void türkçeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DilTurkce(); 
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Eng();
        }

        private void çıkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitDialog();
        }

        public void ExitDialog() 
        {
            DialogResult asd;
            asd = MessageBox.Show(msj, baslik, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (asd == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void screenShotHakkındaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm();
        }

        public void AboutForm()
        {
            Form2 about_form = new Form2();
            about_form.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ScreenShot.Form3 f3 = new ScreenShot.Form3();
            f3.Show();
        }

        private void ayarlarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScreenShot.Ayarlar ayar_form = new ScreenShot.Ayarlar();
            ayar_form.Show();
        }

        /*private void timer1_Tick(object sender, EventArgs e)
        {
           pictureBox1.Image = Görüntü();
        }*/

        
    }
}

