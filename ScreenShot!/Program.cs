﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ScreenShot_
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (ScreenShot.Properties.Settings.Default.splash == true)
            {
                Application.Run(new ScreenShot.Start());
            }
            else 
            {
                Application.Run(new Form1());
            }
        }
    }
}
