﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScreenShot
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
        }

        private void Start_Load(object sender, EventArgs e)
        {
            timer2.Interval = 2000;
            timer2.Start();
            label1.Text = "Hoş Geldiniz! - Welcome!";
            progressBar1.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value += 10;
            }
            if (progressBar1.Value == 100)
            {
                timer1.Stop();
                ScreenShot_.Form1 f1 = new ScreenShot_.Form1();
                f1.Show();
                this.Hide();

            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            label1.Text = "Bileşenler yükleniyor. Lütfen sabredin...\nLoading the components. Please be patient...";
            timer1.Interval = 300;
            timer1.Start();
            timer2.Stop();
        }
    }
}
