﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using ScreenShot.Properties;

namespace ScreenShot_
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        private Process facebook, twitter;

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            facebook = Process.Start("http://www.facebook.com/hilmicanndelibas");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            twitter = Process.Start("https://twitter.com/hilmicanndlbs");
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            if (Settings.Default.Tr == true)
            {
                this.Text = "Hakkında";
                label1.Text = "Sürüm : 3.6.5.0\n\nE-Posta : hilmicandelibas@gmail.com";
                button1.Text = "Tamam";
            }
            else 
            {
                this.Text = "About";
                label1.Text = "Version : 3.6.5.0\n\nE-Mail : hilmicandelibas@gmail.com";
                button1.Text = "OK";
            }
        }
    }
}
