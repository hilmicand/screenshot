﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace English
{
    public class Class1
    {
        public static string menu = "Menu";
        public static string yrdm = "Help";
        public static string yakala = "Take a Picture";
        public static string kaydet = "Save";
        public static string edit = "Edit";
        public static string uyari = "                You haven't take a picture yet!\nIf you have take a pic Your pic will show this place....";
        public static string about = "About";
        public static string dil = "Language";
        public static string cik = "Exit";
        public static string secenek = "Options"; 
        public static string msj_baslik = "Warning";
        public static string msj_text = "If you have not saved picture this pic will be lost\nDo you want continue?";
        public static string baslik = "ScreenShot!";
        public static string msj = "This property has developing so it has not active!";
        public static string tooltip = "Click this icon for take a picture!";
        public static string kydt = "Save this Picture";
        public static string rturn = "Turn the ScreenShot!";
        public static string about_ = "About the ScreenShot!";
        public static string fscr = "FullScreen Mode";
        public static string don = "Close";
        public static string ayar = "Settings";
        public static string dil_ayar = "Language";
        public static string ana_dil = "Main Language";
        public static string splash_kullan = "Use the opening screen (Splash Screen)";
        public static string ayar_kaydet = "Save";
        public static string iptal = "Cancel";
        public static string diger = "Other";
    }
}
