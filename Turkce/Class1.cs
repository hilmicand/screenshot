﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turkce
{
    public class Class1
    {
        public static string menu = "Menü";
        public static string yrdm = "Yardım";
        public static string yakala = "Görüntü Yakala";
        public static string kaydet = "Kaydet";
        public static string edit = "Düzenle";
        public static string uyari = "                Henüz Görüntü Yakalamadınız!\nGörüntü Yakalandığında Bu Pencerede Gösterilecek....";
        public static string about = "Hakkında";
        public static string dil = "Menü Dili";
        public static string cik = "Çıkış";
        public static string secenek = "Seçenekler";
        public static string msj_baslik = "Uyarı";
        public static string msj_text = "Yakaladığınız ekran görüntüsü varsa kaydedilmeyecek\nDevam etmek istiyor musunuz?";
        public static string baslik = "ScreenShot!";
        public static string msj = "Bu özellik geliştirme aşamasında olduğundan şimdilik devre dışı!";
        public static string tooltip = "Ekran görüntüsü almak için bu ikona tıklayın!";
        public static string kydt = "Görüntüyü Kaydet";
        public static string rturn = "ScreenShot!'a Dön";
        public static string about_ = "ScreenShot! Hakkında";
        public static string fscr = "Tam Ekran Modu";
        public static string don = "Kapat";
        public static string ayar = "Ayarlar";
        public static string dil_ayar = "Dil Ayarları";
        public static string ana_dil = "Ana Dil";
        public static string splash_kullan = "Açılış Ekranı (Splash Screen) kullan.";
        public static string ayar_kaydet = "Kaydet";
        public static string iptal = "İptal";
        public static string diger = "Diğer";


    }
}
